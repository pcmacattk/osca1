#include <stdlib.h>
#include <stdio.h>
#include <time.h> //including time for getting random memory allocation
//#include <lib/myfunctions.h>

int main()
{
	srand(time(0));
	char *allocatedmemory;
	char *physmemory = malloc(65536 * sizeof(char)); //physmemory being allocated
	char *virtmemory = malloc(65536 * sizeof(char)); //virtmemory being allocated
	char *pagetable; // pagetable from class

	int frameNumber; // used in writing to files
	int hexIndex; // used in writing to files to track the generated hex address
	int asciiLowCase; 
	int asciiaddition; // number to add to make sure the ascii chars are lower case

	FILE *writingFile;
	writingFile = fopen("data/data.txt", "w+"); //checking if the file is there, if not creates a new one

	if(writingFile == NULL)
	{
		printf("Unable to write to file");
		return 1;
	}

	fprintf(writingFile, "Address | Frame | Content\n");
	fprintf(writingFile, "------- | ----- | -------\n");

	for(int i = 0; i < 514; i++)
	{
		asciiLowCase = (rand() % 25);
		asciiaddition = asciiLowCase + 97;
		fprintf(writingFile,"0x%X  |  %d  |   %c\n",hexIndex,frameNumber,asciiaddition);

		if(i == 256 || i == 512)
		{
			frameNumber++;

		}
		hexIndex++;

	}
	pagetable = (char *) malloc(512); //from the lab, page table
	//getVirtualAddress(address);

	unsigned short address; //hex address using a short for exactly 16bits
	scanf("Enter in a Hex Address: %hx", &address); //getting input for the address
	unsigned short offsetMask = 0x00FF; // offsetMask getting the lowest order 8 bits	unsigned int offset = address & offsetMask;
	unsigned int offset = address & offsetMask; // getting the offset
	unsigned int vpn = address >> 8; // getting the virtual page number by bitshifting
	printf("Address: 0x%X\nOffsetMask : 0x%X\nOffset : 0x%X\nVpn :0x%X\n", address, offsetMask, offset, vpn);


	int randomnummem = rand() % 20480 + 2048;
  allocatedmemory = malloc(randomnummem * sizeof(char));
	if(allocatedmemory == NULL)
	{
		printf("allocatedmemory is empty. Exiting\n");
		return 2;
	}
	printf("rand num = %d",randomnummem);


	fclose(writingFile); // closing the file after the writing has finished

	free(allocatedmemory); //freeing the allocated memory
	return 0;
}
