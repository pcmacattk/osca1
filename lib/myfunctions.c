#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <myfunctions.h>

short getVirtualAddress(unsigned short address)
{
	unsigned short address; //hex address using a short for exactly 16bits
	scanf("Enter in a Hex Address: %hX", &address); //getting input for the address
	unsigned short offsetMask = 0x00FF; // offsetMask getting the lowest order 8 bits of the address
	unsigned int offset = address & offsetMask; // getting the offset
	unsigned int vpn = address >> 8; // getting the virtual page number by bitshifting the address by 8 bits
	printf("Address: 0x%hX\nOffsetMask : 0x%X\nOffset : 0x%X\nVpn :0x%X\n", address, offsetMask, offset, vpn);//printing out the current known values

	return address;
}

