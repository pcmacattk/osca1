CFLAGS = -c
CC = gcc
DISTDIR = dist
LIBDIR = lib
OBJECTS = $(DISTDIR)/myfunctions.o\
					$(DISTDIR)/solution.o

link: $(OBJECTS)
			$(CC) $? -o $(DISTDIR)/solution

dist/solution.o: main.c
			$(CC) $(CFLAGS) $? -o $(DISTDIR)/solution.o

dist/myfunctions.o: $(LIBDIR)/myfunctions.c
			$(CC) $(CFLAGS)  $? -o $(DISTDIR)/myfunctions.o

clean:
			rm -rf ./$(DISTDIR) && mkdir ./$(DISTDIR)

run: link
			./dist/solution
